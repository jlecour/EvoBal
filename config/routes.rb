require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :users

  resources :emails, only: [:index, :show, :destroy] do
    post :batch, on: :collection
  end
  
  resources :metadata_mappings

  resources :filters do
    patch :enable, on: :member
    patch :disable, on: :member
    resources :conditions do
      patch :enable, on: :member
      patch :disable, on: :member
    end
    resources :operations do
      patch :enable, on: :member
      patch :disable, on: :member
    end
  end

  root to: "emails#index"

  mount Sidekiq::Web => '/sidekiq'
end
