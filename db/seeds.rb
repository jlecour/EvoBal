# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(
  email: "jlecour@evolix.fr",
  encrypted_password: Devise::Encryptor.digest(User, '123password')
)

filter = Filter.create(
  description: "CronMapping",
  enabled: true
)
filter.operations.create({
  class_name: "EmailOperation::CronMapping",
  enabled: true
})

filter = Filter.create(
  description: "MailingListMapping",
  enabled: true
)
filter.operations.create({
  class_name: "EmailOperation::MailingListMapping",
  enabled: true
})

filter = Filter.create(
  description: "MetadataMapping",
  enabled: true
)
filter.operations.create({
  class_name: "EmailOperation::MetadataMapping",
  enabled: true
})
