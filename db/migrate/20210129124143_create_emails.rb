class CreateEmails < ActiveRecord::Migration[6.1]
  def change
    create_table :emails do |t|
      t.string :message_id
      t.string :subject
      t.datetime :date
      t.string :to
      t.string :delivered_to
      t.string :from
      t.text :headers
      t.text :plain_body
      t.boolean :cron,          default: false, null: false
      t.boolean :mailing_list,  default: false, null: false
      t.boolean :junk,          default: false, null: false
      t.text  :organisations
      t.text  :servers
      t.text  :issues
      t.datetime :postponed_until
      t.timestamps
    end
  end
end
