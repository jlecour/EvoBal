class CreateFilters < ActiveRecord::Migration[6.1]
  def change
    create_table :filters do |t|
      t.string :description, null: false
      t.boolean :enabled, default: true, null: false
      t.string :operator, default: "AND", limit: 3, null: false
      t.boolean :inverted, default: false, null: false

      t.timestamps
    end
  end
end
