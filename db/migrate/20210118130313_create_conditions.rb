class CreateConditions < ActiveRecord::Migration[6.1]
  def change
    create_table :conditions do |t|
      t.references :filter, null: false, foreign_key: true
      t.boolean :enabled, default: true, null: false
      t.string :property_type, null: false
      t.string :property_value
      t.string :test_method, default: "contain", null: false
      t.string :test_value
      t.boolean :inverted, default: false, null: false

      t.timestamps
    end
  end
end
