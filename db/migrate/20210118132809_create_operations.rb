class CreateOperations < ActiveRecord::Migration[6.1]
  def change
    create_table :operations do |t|
      t.references :filter, null: false, foreign_key: true
      t.boolean :enabled, default: true, null: false
      t.string :class_name, null: false
      t.string :argument

      t.timestamps
    end
  end
end
