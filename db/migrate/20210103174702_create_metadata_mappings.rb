class CreateMetadataMappings < ActiveRecord::Migration[6.1]
  def change
    create_table :metadata_mappings do |t|
      t.string :input
      t.string :server
      t.string :organisation
      t.string :source, limit: 20

      t.timestamps
    end
    add_index :metadata_mappings, :input
  end
end
