class AddInboxToEmails < ActiveRecord::Migration[7.0]
  def up
    add_column :emails, :inbox, :boolean, default: true, null: false
    add_index :emails, :inbox
  end

  def down
    remove_column :emails, :inbox
  end
end
