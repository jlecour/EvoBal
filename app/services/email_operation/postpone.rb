# frozen_string_literal: true

module EmailOperation 
  class Postpone < Base

    def process(email)
      if date = Chronic.parse(operation.argument)
        email.postponed_until = date
      else
        Rails.logger.warn "Skipped operation##{operation.id} '#{operation.class_name}' - Unparsable argument for Chronic : '#{operation.argument}'"
      end

      email
    end

  end
end
