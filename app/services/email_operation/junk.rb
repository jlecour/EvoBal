# frozen_string_literal: true

module EmailOperation 
  class Junk < Base

    def process(email)
      email.junk = true

      email
    end

  end
end
