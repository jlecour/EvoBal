# frozen_string_literal: true

module EmailOperation 
  class IssueMapping < Base

    def process(email)
      values = ["X-Ticket-Id", "X-Issue-Id"].filter_map { |header_name|
        email.header_values(header_name)
      }.flatten.uniq

      email.issues = values

      email
    end

  end
end
