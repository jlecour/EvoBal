# frozen_string_literal: true

module EmailOperation 
  class MailingListMapping < Base

    def process(email)
      if email.header_values("List-Unsubscribe").present?
        email.mailing_list = true
      end

      email
    end

  end
end
