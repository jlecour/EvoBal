# frozen_string_literal: true

module EmailOperation 
  class MetadataMapping < Base

    attr_accessor :metadata_mapping_class

    def initialize(operation:, metadata_mapping_class: ::MetadataMapping)

      @metadata_mapping_class = metadata_mapping_class
      super(operation: operation)
    end

    def process(email)
      metadata_inputs = metadata_inputs(email)
      metadata = metadata_mapping_class.where(input: metadata_inputs).all

      email.organisations = metadata.filter_map(&:organisation).uniq
      email.servers = metadata.filter_map(&:server).uniq

      email
    rescue => ex
      byebug
    end

    private

    def metadata_inputs(email)
      inputs = []

      # add mail addresses and hostnames from headers 
      inputs << ["To", "Delivered-To", "X-Original-To", "From", "Subject"].filter_map { |header_name|
        email.header_values(header_name)
      }.flatten.filter_map() { |header_value|
        keep_email_and_hostnames(header_value)
      }.flatten.filter_map() { |value|
        clean_subdomains(value)
      }.flatten

      # add other values from headers
      inputs << ["X-Client-Id", "X-Organisation-Id"].filter_map { |header_name|
        email.header_values(header_name)
      }.flatten.uniq

      inputs.flatten.uniq
    end

    def keep_email_and_hostnames(string)
      results = string.scan(email_and_hostnames_pattern)

      if results.present?
        results.map(&:first)
      end
    end

    def clean_subdomains(value)
      clean_subdomains_patterns.filter_map { |pattern|
        if value.match?(pattern[:search])
          value.gsub!(pattern[:search], pattern[:replace])
        else
          value
        end
      }
    end

    def email_and_hostnames_pattern
      /\b((?:([-a-zA-Z0-9\._]+)@)?((?:[-a-zA-Z0-9\._]*)(?:\.[a-z]{2,})))\b/
    end

    def clean_subdomains_patterns
      [{
        search: /[-a-zA-Z0-9\._]+@([-a-zA-Z0-9]+).evolix.net/,
        replace: '@\1'
      }]
    end

  end
end
