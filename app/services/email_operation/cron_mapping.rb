# frozen_string_literal: true

module EmailOperation 
  class CronMapping < Base

    def process(email)
      if email.header_values("X-Cron-Env").present?
        email.cron = true
      end
      if email.subject.present? && email.subject.match?(/cron/i)
          email.cron = true
      end

      email
    end

  end
end
