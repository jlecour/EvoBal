# frozen_string_literal: true

module EmailOperation
  
  class Error < ::StandardError
  end

  class Base

    attr_reader :operation

    def initialize(operation:)
      @operation = operation
    end

    def process(email)
      fail NotImplementedError
    end

  end

end
