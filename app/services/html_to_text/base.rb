# frozen_string_literal: true

module HtmlToText
  
  class Error < ::StandardError
  end

  class Base
    def convert(html)
      fail NotImplementedError
    end
  end

end
