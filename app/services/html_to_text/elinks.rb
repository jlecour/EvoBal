# frozen_string_literal: true

module HtmlToText 
  class Elinks < Base
    
    attr_accessor :elinks_path

    def initialize(elinks_path: "/usr/bin/elinks")
      @elinks_path = elinks_path
    end

    def convert(html_input)
      output, error, status = ::Open3.capture3("#{elinks_path} -dump -force-html", stdin_data: html_input)
      if status.success?
        output
      else
        raise Error, "Error calling elinks : #{error}"
      end
    end

  end
end
