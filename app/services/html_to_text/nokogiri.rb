# frozen_string_literal: true

module HtmlToText 
  class Nokogiri < Base

    def convert(html_input)
      ::Nokogiri::HTML(html_input).text
    rescue Exception, ex
      raise Error, "Error using Nokogiri : #{ex.message}"
    end

  end
end
