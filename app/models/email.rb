# frozen_string_literal: true

class Email < ApplicationRecord
  include Elasticsearch::Model

  serialize :from,          Array
  serialize :to,            Array
  serialize :delivered_to,  Array
  serialize :headers,       Array
  serialize :organisations, Array
  serialize :servers,       Array
  serialize :issues,        Array

  scope :inbox, -> { where(inbox: true) }

  paginates_per 10

  def postponed?
    postponed_until.present? && postponed_until > DateTime.now
  end

  def header_values(header_name)
    headers.select { |header|
      header["name"] == header_name
    }.filter_map { |header|
      header["value"]
    }
  end
end
