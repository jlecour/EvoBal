# frozen_string_literal: true

class Filter < ApplicationRecord
  has_many :conditions, dependent: :destroy
  has_many :operations, dependent: :destroy
  
  validates :description, presence: true

  scope :enabled, -> { where(enabled: true) }
end
