# frozen_string_literal: true

class Condition < ApplicationRecord
  belongs_to :filter

  scope :enabled, -> { where(enabled: true) }

  validates :property_type, presence: true
end
