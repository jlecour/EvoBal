# frozen_string_literal: true

class Operation < ApplicationRecord
  belongs_to :filter

  scope :enabled, -> { where(enabled: true) }
end
