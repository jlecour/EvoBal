import { Controller } from "stimulus"

export default class extends Controller {
    static targets = [ "checkboxesGroup", "checkboxesItem" ]

    syncCheckedState() {
        this.checkboxesItemTargets.forEach((element, index) => {
            element.checked = this.groupCheckedState
        })
    }
    get groupCheckedState() {
        return this.checkboxesGroupTarget.checked
    }
}
