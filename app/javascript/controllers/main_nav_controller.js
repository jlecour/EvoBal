import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "userButton", "userMenu" ]

  toggle_user_menu () {
    if (this.userButtonTarget.getAttribute('aria-expanded') == "false") {
      // Show
      this.userButtonTargets.forEach((element, index) => {
        element.setAttribute('aria-expanded', 'true');
      })
      this.userMenuTargets.forEach((element, index) => {
        element.classList.remove("hidden")
      })
    } else {
      // Hide
      this.userButtonTargets.forEach((element, index) => {
        element.setAttribute('aria-expanded', 'false');
      })
      this.userMenuTargets.forEach((element, index) => {
        element.classList.add("hidden")
      })
    }
  }
}
