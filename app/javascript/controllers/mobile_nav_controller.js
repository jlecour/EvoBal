import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "mobileMenu" ]

  show() {
    this.mobileMenuTarget.classList.remove("hidden");
  }
  hide() {
    this.mobileMenuTarget.classList.add("hidden");
  }
}
