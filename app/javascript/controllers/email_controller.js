import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "buttonMain", "buttonAll", "headersMain", "headersAll" ]

  show_main () {
    this.buttonMainTarget.classList.add("hidden");
    this.buttonAllTarget.classList.remove("hidden");
    this.headersMainTarget.classList.remove("hidden");
    this.headersAllTarget.classList.add("hidden");
  }
  show_all () {
    this.buttonMainTarget.classList.remove("hidden");
    this.buttonAllTarget.classList.add("hidden");
    this.headersMainTarget.classList.add("hidden");
    this.headersAllTarget.classList.remove("hidden");
  }
}
