class EmailsController < ApplicationController

  # GET /emails
  def index
    @emails = Email.order(date: :desc).page(params[:page])
  end

  # GET /emails/1
  def show
    @email = Email.find(params[:id])
  end

  # DELETE /emails/1
  def destroy
    @email = Email.find(params[:id])
    @email.destroy
    redirect_to emails_url, notice: 'Email was successfully destroyed.'
  end

  def batch
    case params[:batch_action]
    when "mark_inbox"
      Email.where(id: params[:ids]).update_all(inbox: true)
      flash[:notice] = "#{helpers.pluralize(params[:ids].count,"email","emails")} have been marked « inbox »"
    when "mark_not_inbox"
      Email.where(id: params[:ids]).update_all(inbox: false)
      flash[:notice] = "#{helpers.pluralize(params[:ids].count,"email","emails")} have been marked « not inbox »"
    end
    return_url = params[:return_url].presence || emails_url
    redirect_to return_url
  end
end
