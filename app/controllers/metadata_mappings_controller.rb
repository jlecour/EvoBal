class MetadataMappingsController < ApplicationController
  before_action :set_metadata_mapping, only: [:show, :edit, :update, :destroy]

  # GET /metadata_mappings
  def index
    @metadata_mappings = MetadataMapping.all.order(organisation: :asc, server: :asc, input: :asc)
  end

  # GET /metadata_mappings/1
  def show
  end

  # GET /metadata_mappings/new
  def new
    @metadata_mapping = MetadataMapping.new
  end

  # GET /metadata_mappings/1/edit
  def edit
  end

  # POST /metadata_mappings
  def create
    @metadata_mapping = MetadataMapping.new(metadata_mapping_params)

    if @metadata_mapping.save
      redirect_to @metadata_mapping, notice: 'Metadata mapping was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /metadata_mappings/1
  def update
    if @metadata_mapping.update(metadata_mapping_params)
      redirect_to @metadata_mapping, notice: 'Metadata mapping was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /metadata_mappings/1
  # DELETE /metadata_mappings/1.json
  def destroy
    @metadata_mapping.destroy
    redirect_to metadata_mappings_url, notice: 'Metadata mapping was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_metadata_mapping
      @metadata_mapping = MetadataMapping.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def metadata_mapping_params
      params.require(:metadata_mapping).permit(:input, :server, :organisation)
    end
end
