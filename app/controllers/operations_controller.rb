class OperationsController < ApplicationController
  before_action :set_filter
  before_action :set_operation, only: [:show, :edit, :update, :destroy, :enable, :disable]

  # GET /operations
  def index
    @operations = @filter.operations.all
  end

  # GET /operations/1
  def show
  end

  # GET /operations/new
  def new
    @operation = @filter.operations.new
  end

  # GET /operations/1/edit
  def edit
  end

  # POST /operations
  def create
    @operation = @filter.operations.new(operation_params)

    if @operation.save
      redirect_to @filter, notice: 'Operation was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /operations/1
  def update
    if @operation.update(operation_params)
      redirect_to @filter, notice: 'Operation was successfully updated.'
    else
      render :edit
    end
  end

  def enable
    if @operation.update(enabled: true)
      redirect_to @filter, notice: 'Operation was successfully enabled.'
    else
      render :edit
    end
  end

  def disable
    if @operation.update(enabled: false)
      redirect_to @filter, notice: 'Operation was successfully disabled.'
    else
      render :edit
    end
  end

  # DELETE /operations/1
  def destroy
    @operation.destroy
    redirect_to @filter, notice: 'Operation was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between operations.
    def set_filter
      @filter = Filter.find(params[:filter_id])
    end
    def set_operation
      @operation = @filter.operations.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def operation_params
      params.require(:operation).permit(:enabled, :class_name, :argument)
    end
end
