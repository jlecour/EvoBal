class ConditionsController < ApplicationController
  before_action :set_filter
  before_action :set_condition, only: [:show, :edit, :update, :destroy, :enable, :disable]

  # GET /conditions
  def index
    @conditions = @filter.conditions.all
  end

  # GET /conditions/1
  def show
  end

  # GET /conditions/new
  def new
    @condition = @filter.conditions.new
  end

  # GET /conditions/1/edit
  def edit
  end

  # POST /conditions
  def create
    @condition = @filter.conditions.new(condition_params)

    if @condition.save
      redirect_to @filter, notice: 'Filter was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /conditions/1
  def update
    if @condition.update(condition_params)
      redirect_to @filter, notice: 'Filter was successfully updated.'
    else
      render :edit
    end
  end

  def enable
    if @condition.update(enabled: true)
      redirect_to @filter, notice: 'Condition was successfully enabled.'
    else
      render :edit
    end
  end

  def disable
    if @condition.update(enabled: false)
      redirect_to @filter, notice: 'Condition was successfully disabled.'
    else
      render :edit
    end
  end

  # DELETE /conditions/1
  def destroy
    @condition.destroy
    redirect_to @filter, notice: 'Filter was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between conditions.
    def set_filter
      @filter = Filter.find(params[:filter_id])
    end
    def set_condition
      @condition = @filter.conditions.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def condition_params
      params.require(:condition).permit(:enabled, :property_type, :property_value, :test_method, :test_value, :inverted)
    end
end
