class InMailbox < ApplicationMailbox
  def process
    email_importer = EmailImporter.new

    email = email_importer.import(mail)

    processor = FilterProcessor.new
    email = processor.process_all(Filter.enabled, email)

    # repository = EmailRepository.new
    # repository.save(email)
    email.save!
  end
end
