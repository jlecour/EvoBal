class EmailRepository
  include Elasticsearch::Persistence::Repository
  include Elasticsearch::Persistence::Repository::DSL

  client Elasticsearch::Client.new url: ENV['ELASTICSEARCH_URL'], log: true

  index_name :emails
  klass Email

  mapping do
    indexes :subject,       type: 'text'
    indexes :date,          type: 'date'
    indexes :to,            type: 'text'
    indexes :delivered_to,  type: 'text'
    indexes :from,          type: 'text'
    indexes :plain_body,    type: 'text'
    indexes :raw_headers,   type: 'text', index: 'not_analyzed'
    indexes :headers,       type: 'nested' do
      indexes :name,        type: 'text'
      indexes :value,       type: 'text'
    end
    indexes :message_id,    type: 'keyword'
    indexes :cron,          type: 'boolean'
    indexes :mailing_list,  type: 'boolean'
    indexes :organisations, type: 'keyword'
    indexes :servers,       type: 'keyword'
    indexes :issues,        type: 'keyword'
    indexes :created_at,    type: 'date'
    indexes :updated_at,    type: 'date'
  end

  def deserialize(document)
    Email.new document['_source'].merge('id' => document['_id'])
  end
end
