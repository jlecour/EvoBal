# frozen_string_literal: true

class FormErrorsComponent < ViewComponent::Base
  def initialize(errors:)
    @errors = errors
  end

end
