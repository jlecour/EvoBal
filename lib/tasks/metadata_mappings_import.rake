namespace :metadata_mappings do
  desc "Import metadata mappings"
  task :import => [:environment] do
    ldap = Net::LDAP.new
    ldap.host = "ldap.evolix.net"
    ldap.port = 389
    ldap.auth Rails.application.credentials.ldap[:username], Rails.application.credentials.ldap[:password]

    if ldap.bind
      metadata = {}

      filter = "(&(objectClass=EvoClient)(isActive=TRUE))"
      treebase = "ou=clients,dc=evolix,dc=net"
      ldap.search(:base => treebase, :filter => filter) do |entry|
        client_number = entry[:clientnumber][0]
        client_name = entry[:clientevoid][0]

        if client_name.present?
          metadata[client_number] = {organisation: client_name}
          entry['techmail'].each { |techmail|
            metadata[techmail] = {server: nil, organisation: client_name}
          }
        end
      end

      filter = "(&(objectClass=EvoComputer)(isActive=TRUE))"
      treebase = "ou=computer,dc=evolix,dc=net"
      ldap.search(:base => treebase, :filter => filter) do |entry|
        client_number = entry[:clientnumber][0]
        server = entry[:evocomputername][0]

        if metadata.has_key?(client_number)
          client_name = metadata[client_number][:organisation]
          inputs = []
          if "#{entry[:dnsarecord]}."
            inputs << "@#{entry[:dnsarecord][0]}"
          end
          if "#{entry[:evocomputername]}."
            inputs << "@#{entry[:evocomputername][0]}"
          end
          if entry[:dnsptrrecord] && entry[:dnsarecord]
            unless entry[:dnsptrrecord][0].match?(entry[:dnsarecord][0])
              inputs << "@#{entry[:dnsptrrecord][0]}"
            end
          end

          inputs.uniq.each do |input|
            if metadata.has_key?(input)
              puts "input already found for #{input}"
            else
              metadata[input] = {server: server, organisation: client_name}
            end
          end
        end
      end

      metadata.each do |input, meta|
        mapping = MetadataMapping.find_or_initialize_by(input: input)
        mapping.server = meta[:server]
        mapping.organisation = meta[:organisation]
        mapping.source = :ldap
        mapping.save
      end

    else
      puts "LDAP authentication failed." 
    end

  end
end
