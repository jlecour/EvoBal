ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  # parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  def email_from_eml(file_fixture_name)
    eml = file_fixture(file_fixture_name).read
    mail = Mail.from_source(eml)
    email_importer = EmailImporter.new
    email = email_importer.import(mail)

    email
  end
  def email_from_eml_with_filters(file_fixture_name)
    email = email_from_eml(file_fixture_name)
    processor = FilterProcessor.new
    email = processor.process_all(Filter.enabled, email)
  end

  def assert_no_html(text)
    assert_no_match(/<\/?(p|b|br|img)\/?>/, text)
  end
end
