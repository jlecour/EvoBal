require "test_helper"

class MetadataMappingsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:alice)
    @metadata_mapping = metadata_mappings(:one)
  end

  test "should get index" do
    get metadata_mappings_url
    assert_response :success
  end

  test "should get new" do
    get new_metadata_mapping_url
    assert_response :success
  end

  test "should create metadata_mapping" do
    assert_difference('MetadataMapping.count') do
      post metadata_mappings_url, params: { metadata_mapping: { organisation: @metadata_mapping.organisation, input: @metadata_mapping.input, server: @metadata_mapping.server } }
    end

    assert_redirected_to metadata_mapping_url(MetadataMapping.last)
  end

  test "should show metadata_mapping" do
    get metadata_mapping_url(@metadata_mapping)
    assert_response :success
  end

  test "should get edit" do
    get edit_metadata_mapping_url(@metadata_mapping)
    assert_response :success
  end

  test "should update metadata_mapping" do
    patch metadata_mapping_url(@metadata_mapping), params: { metadata_mapping: { organisation: @metadata_mapping.organisation, input: @metadata_mapping.input, server: @metadata_mapping.server } }
    assert_redirected_to metadata_mapping_url(@metadata_mapping)
  end

  test "should destroy metadata_mapping" do
    assert_difference('MetadataMapping.count', -1) do
      delete metadata_mapping_url(@metadata_mapping)
    end

    assert_redirected_to metadata_mappings_url
  end
end
