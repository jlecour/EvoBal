require "application_system_test_case"

class MetadataMappingsTest < ApplicationSystemTestCase
  setup do
    @metadata_mapping = metadata_mappings(:one)
  end

  test "visiting the index" do
    visit metadata_mappings_url
    assert_selector "h1", text: "Metadata Mappings"
  end

  test "creating a Metadata mapping" do
    visit metadata_mappings_url
    click_on "New Metadata Mapping"

    fill_in "Organisation", with: @metadata_mapping.organisation
    fill_in "Input", with: @metadata_mapping.input
    fill_in "Server", with: @metadata_mapping.server
    click_on "Create Metadata mapping"

    assert_text "Metadata mapping was successfully created"
    click_on "Back"
  end

  test "updating a Metadata mapping" do
    visit metadata_mappings_url
    click_on "Edit", match: :first

    fill_in "Organisation", with: @metadata_mapping.organisation
    fill_in "Input", with: @metadata_mapping.input
    fill_in "Server", with: @metadata_mapping.server
    click_on "Update Metadata mapping"

    assert_text "Metadata mapping was successfully updated"
    click_on "Back"
  end

  test "destroying a Metadata mapping" do
    visit metadata_mappings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Metadata mapping was successfully destroyed"
  end
end
