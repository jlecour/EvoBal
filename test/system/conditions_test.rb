# require "application_system_test_case"
# 
# class ConditionsTest < ApplicationSystemTestCase
#   setup do
#     @condition = conditions(:one)
#   end
# 
#   test "visiting the index" do
#     visit conditions_url
#     assert_selector "h1", text: "Conditions"
#   end
# 
#   test "creating a Condition" do
#     visit conditions_url
#     click_on "New Condition"
# 
#     fill_in "Property type", with: @condition.property_type
#     fill_in "Property value", with: @condition.property_value
#     check "Enabled" if @condition.enabled
#     check "Inverted" if @condition.inverted
#     fill_in "Test method", with: @condition.test_method
#     fill_in "Test value", with: @condition.test_value
#     click_on "Create Condition"
# 
#     assert_text "Condition was successfully created"
#     click_on "Back"
#   end
# 
#   test "updating a Condition" do
#     visit conditions_url
#     click_on "Edit", match: :first
# 
#     fill_in "Property type", with: @condition.property_type
#     fill_in "Property value", with: @condition.property_value
#     check "Enabled" if @condition.enabled
#     check "Inverted" if @condition.inverted
#     fill_in "Test method", with: @condition.test_method
#     fill_in "Test value", with: @condition.test_value
#     click_on "Update Condition"
# 
#     assert_text "Condition was successfully updated"
#     click_on "Back"
#   end
# 
#   test "destroying a Condition" do
#     visit conditions_url
#     page.accept_confirm do
#       click_on "Destroy", match: :first
#     end
# 
#     assert_text "Condition was successfully destroyed"
#   end
# end
