require 'test_helper'

class EmailImporterTest < ActiveSupport::TestCase

  test "convert html to text when not multipart html only" do
    email = email_from_eml("html_only.eml")

    expected = /Attention, plus que quelques jours pour/

    assert_match(expected, email.plain_body)
    assert_no_html email.plain_body
  end

  test "convert html to text when multipart and html only" do
    email = email_from_eml("html_only_multipart.eml")

    expected = /Complément d''information : suite du ticket 2009P88471/

    assert_match(expected, email.plain_body)
    assert_no_html email.plain_body
  end

  test "convert html to text when base64-encoded html only" do
    email = email_from_eml("html_only_base64.eml")
  
    expected = /Type: Health and Safety/
  
    assert_match(expected, email.plain_body)
    assert_no_html email.plain_body
  end
  
  test "single delivered-to" do
    email = email_from_eml("delivered_to_single.eml")
  
    expected = ["delivered-to-1@example.com"]
    actual   = email.delivered_to
  
    assert_equal expected, actual
  end
  
  test "multiple delivered-to" do
    email = email_from_eml("delivered_to_multiple.eml")
  
    expected = ["delivered-to-1@example.com", "delivered-to-2@example.com", "delivered-to-3@example.com"]
    actual   = email.delivered_to
  
    assert_equal expected, actual
  end
  
  test "missing delivered-to fallback to To field" do
    email = email_from_eml("delivered_to_missing.eml")
  
    expected = ["to-foo@example.com"]
    actual   = email.delivered_to
  
    assert_equal expected, actual
  end
  
  test "mail without content-type" do
    email = email_from_eml("no_content_type.eml")
  
    expected = /This is a RAID status update from megaraidsas-statusd/
  
    assert_match(expected, email.plain_body)
  end

end
