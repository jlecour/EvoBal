require 'test_helper'

class FilterProcessorTest < ActiveSupport::TestCase

  test "mark cron from subject" do
    email = email_from_eml_with_filters("cron_subject.eml")

    assert_predicate email, :cron?
  end

  test "mark cron from headers" do
    email = email_from_eml_with_filters("cron_headers.eml")

    assert_predicate email, :cron?
  end

  test "mark not cron" do
    email = email_from_eml_with_filters("cron_not.eml")

    assert_not_predicate email, :cron?
  end

  test "single issue" do
    email = email_from_eml_with_filters("issues_single.eml")

    expected = ["49123"]
    actual   = email.issues

    assert_equal expected, actual
  end

  test "multiple issues" do
    email = email_from_eml_with_filters("issues_multiple.eml")

    expected = ["49123", "12345"]
    actual   = email.issues

    assert_equal expected, actual
  end

  test "single organisation" do
    email = email_from_eml_with_filters("organisations_single.eml")

    expected = ["quux"]
    actual   = email.organisations

    assert_equal expected, actual
  end

  test "multiple organisations" do
    email = email_from_eml_with_filters("organisations_multiple.eml")

    expected = ["quux", "foobar"]
    actual   = email.organisations

    assert_equal expected, actual
  end

  test "invalid Test" do
    email = Email.new
    processor = FilterProcessor.new
    email = processor.process(filters(:invalid_subject), email)

    assert_not_predicate email, :changed?
  end

  test "invalid Property type" do
    email = Email.new
    processor = FilterProcessor.new
    email = processor.process(filters(:invalid_condition_type), email)

    assert_not_predicate email, :changed?
  end

  test "invalid operator" do
    email = Email.new
    processor = FilterProcessor.new
    email = processor.process(filters(:invalid_operator), email)

    assert_not_predicate email, :changed?
  end

  test "postponed to valid future date" do
    email = Email.new(subject: "Postponable")
    processor = FilterProcessor.new
    email = processor.process(filters(:postpone_future_valid), email)

    assert_not_nil email.postponed_until
    assert_predicate email, :postponed?
  end
  
  test "postponed to valid past date" do
    email = Email.new(subject: "Postponable")
    processor = FilterProcessor.new
    email = processor.process(filters(:postpone_past_valid), email)
  
    assert_not_nil email.postponed_until
    assert_not_predicate email, :postponed?
  end
  
  test "postponed to invalid date" do
    email = Email.new(subject: "Postponable")
    processor = FilterProcessor.new
    email = processor.process(filters(:postpone_invalid), email)
  
    assert_nil email.postponed_until
    assert_not_predicate email, :postponed?
  end
  
  test "junk mail" do
    email = Email.new(subject: "Junk")
    processor = FilterProcessor.new
    email = processor.process(filters(:junk), email)
  
    assert_predicate email, :junk?
  end

end
