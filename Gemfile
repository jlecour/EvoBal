source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '>= 2.7.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
# gem 'rails', '~> 6.1.0'
gem 'rails', github: 'rails/rails', branch: 'main'

# Use sqlite3 as the database for Active Record
gem 'sqlite3', '~> 1.4'

# Use Puma as the app server
gem 'puma', '~> 5.1'

# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'

# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.0'

# Use Hotwire for the frontend
gem 'hotwire-rails'

# Use Tailwind CSS for frontend design
gem 'tailwindcss-rails'

# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'

# Use Active Model has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Use chronic to parse dates
gem 'chronic'

# Use Nokogiri to transform HTML to text
gem 'nokogiri', "~> 1.11.0"

# some libraries is no longer default gems in Ruby 3.0
# https://github.com/rails/rails/commit/c23533ee0b50fdc67cc73b579674637ba6f34cb4
gem 'rexml'
gem 'open3'

# Use Elasticsearch as Search database
gem 'elasticsearch-model'
gem 'elasticsearch-rails'

# Use Sidekiq for background queues
gem 'sidekiq'

# Use Net::LDAP to fetch data from an LDAP server
gem 'net-ldap'

# Use Devise for authentication
gem 'devise'

# Use Kaminari for pagination
gem "kaminari"

# Use ViewComponents for … view components, duh!
gem "view_component", require: "view_component/engine"

# Use heroicon to display SVG icons from heroicons.com
gem "heroicon"

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'listen', '~> 3.3'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
