# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

Testé principalement avec Ruby 3.0 mais probabement compatible avec des versions précédentes (2.7, 2.5…).

* System dependencies

Elasticsearch
Redis
une base SQL (SQLite, MySQL/MariaDB ou PostgreSQL).
Elinks en option (pour tranformer le HTML en texte brut)

* Configuration

* Database creation

```
rails db:migrate
```

* Database initialization

```
rails db:seed
```

Pour injecter les metadonnées issues de LDAP :

```
rails metadata_mappings:import
```

Pour injecter manuellement des mails dans le système (le serveur Rails doit tourner) :

```
for file in tmp/equipe-select/*.eml; do cat "${file}" | rails action_mailbox:ingress:postfix URL=http://0.0.0.0:3000/rails/action_mailbox/relay/inbound_emails INGRESS_PASSWORD=XXXXXXXX; done
```

* How to run the test suite

```
rails test
```

* Services (job queues, cache servers, search engines, etc.)

```
bundle exec sidekiq
```

* Deployment instructions

* ...
